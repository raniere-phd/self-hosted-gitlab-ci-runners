# Self-Hosted GitLab CI Runners

This is a learning exercise about self-hosted GitLab CI runners.
`.gitlab-ci.yml` is a minimal example
that is succesfuly executed in GitLab.

![Screenshot of GitLab Pipeline](img/pipelines.png)

In `Settings > CI/CD > Runners`,
a list of hosts is provid.

![Screenshot of GitLab list of hosts](img/list-hosts.png)

## Install GitLab Runner

It is possible to install GitLab Runner as a binary in Ubuntu.

```
$ curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
$ sudo dpkg -i gitlab-runner_amd64.deb
```

Check that GitLab Runner is running:

```
$ sudo gitlab-runner status
Runtime platform                                    arch=amd64 os=linux pid=282993 revision=98daeee0 version=14.7.0
gitlab-runner: Service is running
```

If GitLab Runner is not running,
it can be started:

```
$ sudo gitlab-runner start
```

## Registering a Runner

After installing,
the runner must be registered.
One host machine can have multiple runners.

```
$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=282821 revision=98daeee0 version=14.7.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
*******************
Enter a description for the runner:
[raniere]: test
Enter tags for the runner (comma-separated):
test
Registering runner... succeeded                     runner=QEQw1Ps2
Enter an executor: parallels, shell, ssh, docker+machine, kubernetes, docker, docker-ssh, virtualbox, docker-ssh+machine, custom:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Check that the runner was registered:

```
$ sudo gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=283024 revision=98daeee0 version=14.7.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
test                                                Executor=shell Token=******************** URL=https://gitlab.com/
```

The registered runner should also be listed in
`Settings > CI/CD > Runners`.

![Screenshot of GitLab list of hosts with private one](list-hosts-with-private.png)

To unregister a runner,
run

```
sudo gitlab-runner unregister -n test
Runtime platform                                    arch=amd64 os=linux pid=291430 revision=98daeee0 version=14.7.0
Running in system-mode.                            
                                                   
Unregistering runner from GitLab succeeded          runner=yr4yAxar
Updated /etc/gitlab-runner/config.toml
```

## Selection of Runner

A runner is select to carry with the job
if it has all the values listed in the `tags` field of `.gitlab-ci.yml`.

## Add Environment Variables

Environment variables can be defined in `/etc/gitlab-runner/config.toml`.
For example

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "test"
  url = "https://gitlab.com/"
  token = "********************"
  executor = "shell"
  environment = ["mydata=/home"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
```